
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mariusz
 */
public class FileOpener {
    private JFileChooser fileChooser;
    private StringBuilder sb;

    public FileOpener() {
        this.fileChooser = new JFileChooser();
        this.sb = new StringBuilder();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
        this.fileChooser.setFileFilter(filter);
    }
    
    public void pickFile() throws FileNotFoundException {
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            Scanner input = new Scanner(file);
            
            while (input.hasNext()) {
                sb.append(input.nextLine()).append("\n");
            }
            input.close();
        }
    }

    public String getSbToString() {
        return sb.toString();
    }
}
