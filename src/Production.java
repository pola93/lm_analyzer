
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mariusz
 */
public class Production {
    private String productionName;
    private List<Alternative> alternatives;

    public Production(String productionName, String alternatives) {
        this.productionName = productionName;
        this.alternatives = new ArrayList<>();
        String[] altParts = alternatives.split(" \\| ");
        for (String altPart : altParts) {
            this.alternatives.add(new Alternative(altPart));
        }
    }
    
    public Production(String productionName, List<Alternative> alternatives) {
        this.productionName = productionName;
        this.alternatives = new ArrayList<>(alternatives);
    }

    public String getProductionName() {
        return productionName;
    }

    public List<Alternative> getAlternatives() {
        return new ArrayList<>(alternatives);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(productionName).append("::= ");
        for (Alternative alternative : alternatives) {
            for (String item : alternative.getItems()) {
                sb.append(item).append(" ");
            }
            if(alternatives.indexOf(alternative) != (alternatives.size() -1)) {
                sb.append("| ");
            }
        }
        return sb.toString();
    }
}
