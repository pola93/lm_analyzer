
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mariusz
 */
public class Alternative {
    private List<String> items;

    public Alternative(String item) {
        this.items = new ArrayList<>();
        String[] itemParts = item.split(" ");
        for (String itemPart : itemParts) {
            this.items.add(itemPart);
        }
    }
    
    public Alternative(List<String> it) {
        this.items = new ArrayList<>(it);
    }
    
    public Alternative(Alternative alt) {
        this.items = new ArrayList<>(alt.getItems());
    }

    public List<String> getItems() {
        return new ArrayList<>(items);
    }
    
    public String getFirstItem(){
        return items.get(0);
    }
    
    public List<String> getFollowingItems(String givenItem) {
        ListIterator<String> it = this.items.listIterator();
        List<String> follows = new ArrayList<>();
        while (it.hasNext()) {
            String next = it.next();
            if(next.equals(givenItem) && it.hasNext()){
                follows.add(it.next());
            }
        }
        return follows;
    }
    
    public List<String> getSublist(int fromIndex) {
        return new ArrayList<>(items.subList(fromIndex, items.size()));
    }
    
    public void addPrimProdName(String prodName) {
        items.add(prodName + "'");
    }
}
