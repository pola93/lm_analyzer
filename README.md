# LM_Analyzer

### Analizator syntaktyczny na zaliczenie laboratorium z Lingwistyki Matematycznej ###

* Wczytywanie gramatyki z pliku tekstowego
* Wyznaczanie *Firsts* i *Follows* dla podanej gramatyki
* Sprawdzanie pierwszej i drugiej reguły
* Wyznaczanie poprawnej gramatyki poprzez na podstawie *Left common factor* oraz *Left recursion*
* Sprawdzanie czy podane zdanie można wyprowadzić z poprawionej gramatyki

***Przykładowe gramatyki i zdania dołączone do repozytorium***
## License
Project is available under "THE BEER-WARE LICENSE" (Revision 42).